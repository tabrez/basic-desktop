code --install-extension GitHub.github-vscode-theme
# code --install-extension wesbos.theme-cobalt2
code --install-extension vscode-icons-team.vscode-icons
code --install-extension VisualStudioExptTeam.vscodeintellicode
# code --install-extension formulahendry.auto-rename-tag
# code --install-extension formulahendry.auto-close-tag
code --install-extension steoates.autoimport
# code --install-extension ms-edgedevtools.vscode-edge-devtools
# code --install-extension  msjsdiag.debugger-for-edge
code --install-extension esbenp.prettier-vscode
code --install-extension dbaeumer.vscode-eslint
code --install-extension stkb.rewrap
code --install-extension eamodio.gitlens
code --install-extension formulahendry.code-runner
code --install-extension Centuplex.quokka
# code --install-extension MS-vsliveshare.vsliveshare
code --install-extension ckolkman.vscode-postgres
code --install-extension humao.rest-client
code --install-extension 42Crunch.vscode-openapi
code --install-extension firsttris.vscode-jest-runner
code --install-extension wix.vscode-import-cost

# code --install-extension 4ops.packer
# code --install-extension marcostazi.VS-code-vagrantfile
# code --install-extension 4ops.terraform
code --install-extension redhat.ansible
# code --install-extension robby.poe-filter
code --install-extension vscodevim.vim
code --install-extension DavidAnson.vscode-markdownlint

# code --install-extension ms-python.python
# code --install-extension ms-python.vscode-pylance
# code --install-extension bungcip.better-toml
# code --install-extension knowsuchagency.pdm-task-provider

code --install-extension ms-vscode-remote.remote-containers
code --install-extension ms-vscode-remote.remote-ssh
code --install-extension ms-vscode-remote.remote-ssh-edit
# code --install-extension ms-vscode-remote.remote-wsl
code --install-extension ms-azuretools.vscode-docker
code --install-extension exiasr.hadolint
# code --install-extension ms-kubernetes-tools.vscode-kubernetes-tools

# code --install-extension gitpod.gitpod-desktop
# code --install-extension ms-vscode.powershell
code --install-extension  redhat.vscode-yaml

